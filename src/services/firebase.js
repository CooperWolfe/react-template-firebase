import firebase from 'firebase/app'

import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const config = {
  apiKey: '****',
  authDomain: '****.firebaseapp.com',
  databaseURL: "https://****.firebaseio.com",
  projectId: "****",
  storageBucket: "****.appspot.com",
  messagingSenderId: "****"
};
firebase.initializeApp(config)
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase
